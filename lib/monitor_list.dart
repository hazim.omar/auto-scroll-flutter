import 'package:anchor_list/rect_getter.dart';
import 'package:flutter/material.dart';

class MonitorList extends StatefulWidget {
  @override
  _MonitorListState createState() => _MonitorListState();
}

class _MonitorListState extends State<MonitorList> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              height: 100,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 10,
                itemBuilder: (context, index){
                  return Container(
                    color: Colors.primaries[index % Colors.primaries.length],
                    height: 100,
                    width: 100,
                    child: Center(
                      child: Text("$index"),
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: MyRectGetter(title: "scroll",),
            )
          ],
        ),
      ),
    );
  }
}
