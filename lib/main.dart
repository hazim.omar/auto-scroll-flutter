import 'package:anchor_list/monitor_list.dart';
import 'package:anchor_list/rect_getter.dart';
import 'package:anchor_list/scroll_index.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Scroll To Index Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyRectGetter(title: "scroll",),
    );
  }
}