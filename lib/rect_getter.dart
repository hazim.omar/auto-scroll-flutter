import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rect_getter/rect_getter.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class MyRectGetter extends StatefulWidget {
  MyRectGetter({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyRectGetterState createState() => new _MyRectGetterState();
}

class _MyRectGetterState extends State<MyRectGetter> {
  var _keys = {};
  AutoScrollController controller;
  AutoScrollController topListController;
  final scrollDirection = Axis.vertical;
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    controller = AutoScrollController(
      viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection,
    );

    topListController = AutoScrollController(
      axis: Axis.horizontal
    );
  }

  @override
  Widget build(BuildContext context) {
    /// Make the entire ListView have the ability to get rect.
    var listViewKey = RectGetter.createGlobalKey();

    Widget _getRow(int index){
      return RectGetter(
        key: _keys[index],
        child: AutoScrollTag(
          key: ValueKey(index),
          controller: controller,
          index: index,
          highlightColor: Colors.black.withOpacity(0.8),
          child: Container(
            height: 400,
            color: Colors.primaries[index % Colors.primaries.length],
            child: Center(
              child: Text(index.toString()),
            ),
          )
        ),
      );
    }

    var listView = RectGetter(
      key: listViewKey,
      child: ListView.builder(
        padding: EdgeInsets.only(bottom: 400),
        scrollDirection: scrollDirection,
        controller: controller,
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          /// Make every item have the ability to get rect,
          /// and save keys for RectGetter and its index into _keys.
          _keys[index] = RectGetter.createGlobalKey();
          return _getRow(index);
        },
      ),
    );

    List<int> getVisible() {
      /// First, get the rect of ListView, and then traver the _keys
      /// get rect of each item by keys in _keys, and if this rect in the range of ListView's rect,
      /// add the index into result list.
      var rect = RectGetter.getRectFromKey(listViewKey);
      var _items = <int>[];
      _keys.forEach((index, key) {
        var itemRect = RectGetter.getRectFromKey(key);
        if (itemRect != null && !(itemRect.top > rect.bottom || itemRect.bottom < rect.top)) _items.add(index);
      });

      /// so all visible item's index are in this _items.
      return _items;
    }

    void _scrollTopList(int index) async {
      await topListController.scrollToIndex(index, preferPosition: AutoScrollPosition.middle, duration: Duration(milliseconds: 100));
    }

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Container(
              height: 100,
              child: ListView.builder(
                controller: topListController,
                scrollDirection: Axis.horizontal,
                itemCount: 10,
                itemBuilder: (context, index){
                  return AutoScrollTag(
                    key: ValueKey(index),
                    controller: topListController,
                    index: index,
                    highlightColor: Colors.black.withOpacity(0.8),
                    child: InkWell(
                      onTap: () async{
                        await controller.scrollToIndex(index, preferPosition: AutoScrollPosition.begin, duration: Duration(milliseconds: 100));
                      },
                      child: Container(
                        color: Colors.primaries[index % Colors.primaries.length],
                        height: 100,
                        width: 100,
                        child: Center(
                          child: Text("$index"),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: NotificationListener<ScrollUpdateNotification>(
                onNotification: (notification) {
                  int firstIndex = getVisible().first;
                  if(firstIndex != currentIndex){
                    _scrollTopList(firstIndex);
                    currentIndex = firstIndex;
                  }
                  return true;
                },
                child: listView,
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async{
            await controller.scrollToIndex(8, preferPosition: AutoScrollPosition.begin);
            controller.highlight(8, highlightDuration: Duration(milliseconds: 500));
          },
        ),
      ),
    );
  }
}